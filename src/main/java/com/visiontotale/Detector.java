package com.visiontotale;

import lombok.extern.slf4j.Slf4j;

import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.layers.objdetect.DetectedObject;
import org.deeplearning4j.nn.layers.objdetect.Yolo2OutputLayer;
import org.deeplearning4j.util.ModelSerializer;
import org.deeplearning4j.zoo.model.TinyYOLO;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import java.io.File;
import java.util.List;

@Slf4j
public class Detector {
    Params params = new Params();
    public void execute(String[] args) throws Exception {
        ComputationGraph model;
        String modelFilename = "Object_Detection_Model.zip";

        if (new File(modelFilename).exists()) {
            log.info("Loading existing model...");
            model = ModelSerializer.restoreComputationGraph(modelFilename);
        } else {
            log.info("Downloading the pretrained model...");
            model = (ComputationGraph) TinyYOLO.builder().build().initPretrained();
            if (params.isCanSave()) {
                ModelSerializer.writeModel(model, modelFilename, true);
            }
        }

        log.info(
                model.summary(
                        InputType.convolutional(
                                params.getHeight(),
                                params.getWidth(),
                                params.getNChannels()
                        )
                )
        );

        NativeImageLoader loader = new NativeImageLoader(params.getHeight(),
                params.getWidth(),
                params.getNChannels());
        ImagePreProcessingScaler imagePreProcessingScaler = new ImagePreProcessingScaler(0, 1);
        Yolo2OutputLayer outputLayer = (Yolo2OutputLayer) model.getOutputLayer(0);

        long startTime = System.currentTimeMillis();
        File imageFile = new File("Cars1.jpg");
        INDArray indArray = loader.asMatrix(imageFile);
        imagePreProcessingScaler.transform(indArray);
        INDArray results = model.outputSingle(indArray);
        List<DetectedObject> detectedObjects = outputLayer.getPredictedObjects(results, params.getDetectionThreshold());
        long endTime = System.currentTimeMillis();

        log.info(detectedObjects.size() + " objects detected in "+(endTime-startTime)+" milliseconds");

        new Utils().drawDetectedObjects(imageFile, detectedObjects);  //Drawing boxes on detected objects
    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
        new Detector().execute(args);
    }
}
