package com.visiontotale;

import org.deeplearning4j.nn.layers.objdetect.DetectedObject;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import java.util.List;

public class Utils {

    public void drawDetectedObjects(File imageFile,
                                    List<DetectedObject> detectedObjects) throws IOException {

        Params params = new Params();
        BufferedImage img = ImageIO.read(imageFile);
        Graphics2D g2d = img.createGraphics();
        g2d.setColor(Color.GREEN);
        g2d.setStroke(new BasicStroke(2));

        for (DetectedObject detectedObject : detectedObjects) {
            double x1 = detectedObject.getTopLeftXY()[0];
            double y1 = detectedObject.getTopLeftXY()[1];
            double x2 = detectedObject.getBottomRightXY()[0];
            double y2 = detectedObject.getBottomRightXY()[1];
            int xs1 = (int) ((x1 / 13.0 ) * (double) img.getWidth());
            int ys1 = (int) ((y1 / 13.0 ) * (double) img.getHeight());
            int xs2 = (int) ((x2 / 13.0 ) * (double) img.getWidth());
            int ys2 = (int) ((y2 / 13.0 ) * (double) img.getHeight());
            g2d.drawString(params.getLabels()[detectedObject.getPredictedClass()], xs1+4, ys2-2);
            g2d.drawRect(xs1, ys1, xs2-xs1, ys2-ys1);
        }
        JLabel picLabel = new JLabel(new ImageIcon(img));
        JOptionPane.showMessageDialog(null, picLabel, "Image", JOptionPane.PLAIN_MESSAGE, null);
        g2d.dispose();
    }
}
