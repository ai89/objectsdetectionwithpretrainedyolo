package com.visiontotale;

import lombok.Getter;

@Getter
public class Params {
    private String[] labels = {
            "aeroplane",
            "bicycle",
            "bird",
            "boat",
            "bottle",
            "bus",
            "car",
            "cat",
            "chair",
            "cow",
            "dining table",
            "dog",
            "horse",
            "motorbike",
            "person",
            "potted plant",
            "sheep",
            "sofa",
            "train",
            "tvmonitor"
    };

    private double detectionThreshold = 0.5;
    private int width = 416;
    private int height = 416;
    private int nChannels = 3;
    private boolean canSave = true;
}
